import java.util.Scanner;

public class Zadanie1_2_b {

	public static void main(String[] args) {

		System.out.println("Give a real number: ");
		Scanner sc = new Scanner(System.in);
		float number = sc.nextFloat();

		System.out.println(Math.abs(number));

		sc.close();
	}

}
