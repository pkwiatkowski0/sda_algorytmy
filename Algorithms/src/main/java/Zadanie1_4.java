import java.math.BigInteger;
import java.util.Scanner;

//Zad1.4
//Napisz program, który wyznaczy największy wspólny dzielnik dwóch liczb naturalnych

public class Zadanie1_4 {

	public static void main(String[] args) {

		System.out.println("Give 2 natural numbers in order to find their greatest common devisor");
		Scanner sc = new Scanner(System.in);
		System.out.print("give number1: ");
		int number1 = sc.nextInt();
		System.out.print("give number2: ");
		int number2 = sc.nextInt();
		compareArgumentValues(number1, number2);

		sc.close();
	}

	private static void compareArgumentValues(int number1, int number2) {
		if (number1 < number2) {
			System.err.println("Number1 is smaller than number2");
		} else {
			System.out.println("Greatest common devisor of " + number1 + " and " + number2 + " is "
					+ greatestCommonDevisor(number1, number2));
		}
	}

	private static int greatestCommonDevisor(int a, int b) {
		BigInteger b1 = BigInteger.valueOf(a);
		BigInteger b2 = BigInteger.valueOf(b);
		BigInteger gcd = b1.gcd(b2);
		return gcd.intValue();
	}
}
