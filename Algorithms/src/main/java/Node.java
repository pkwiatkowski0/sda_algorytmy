
public class Node {
	private Object mValue;
	private Node mNext; // wskazuje na następny element w liście
	// gettery i settery zostały wprowadzone żeby wprowadzić enkapsulację i nic
	// ponad to

	public Node(Object Value) {
		mNext = null;
		this.mValue = Value;
	}

	public Object getmValue() {
		return mValue;
	}

	public void setmValue(Object Value) {
		this.mValue = Value;
	}

	public Node getmNext() {
		return mNext;
	}

	public void setmNext(Node Next) {
		this.mNext = Next;
	}

	public Object getData() {
		return mValue;
	}
}
