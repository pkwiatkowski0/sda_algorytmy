import java.util.Scanner;

public class Zadanie2_6 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Podaj wyraz: ");
		String text = sc.nextLine();
		isTextPalindrome(text);
		// }
		sc.close();
	}

	private static boolean isTextPalindrome(String isText) {

		if (isText == null) {
			return false;
		}
		int left = 0;
		int right = isText.length()-1;
		while (left < right) {
			if (isText.charAt(left++) != isText.charAt(right--)) {
				System.err.println("Wyraz nie jest palindromem");
				return false;
			}
			
		}
		System.out.println("hura");
		return true;
	}
}