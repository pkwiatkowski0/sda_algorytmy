import java.util.Scanner;

public class Zadanie1_1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Give an integer number: ");
		int number = sc.nextInt();

		if (number % 2 == 0) {
			System.out.println("Number is even");
		} else {
			System.out.println("Number is odd");
		}

		sc.close();
	}

}
