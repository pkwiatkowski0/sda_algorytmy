
public class listMain {

	// private static Object mValue;

	public static void main(String[] args) {

		Lista linkedList = new Lista();
		// Node node = new Node();
		// node.setmValue(mValue);
		// node.getmNext(Lista.class.);
		linkedList.add("23");
		linkedList.add("223");
		linkedList.add("1");
		linkedList.add("3");
		linkedList.add("4");
		linkedList.add("875");

		System.out.println("Print name of list: \t" + linkedList.getClass().getName());
		System.out.println(".element[0]: \t" + linkedList.get(0));
		System.out.println(".element[1]: \t" + linkedList.get(1));
		System.out.println(".element[2]: \t" + linkedList.get(2));
		System.out.println(".element[3]: \t" + linkedList.get(3));
		System.out.println(".element[4]: \t" + linkedList.get(4));
		System.out.println(".element[5]: \t" + linkedList.get(5));
		System.out.println(".element[6]: \t" + linkedList.get(6));
		System.out.println(".element[-1]: \t" + linkedList.get(-1));
		System.out.println(".size(): \t" + linkedList.getmListSize());

		linkedList.remove(2);

		System.out.println(".element[0]: \t" + linkedList.get(0));
		System.out.println(".element[1]: \t" + linkedList.get(1));
		System.out.println(".element[2]: \t" + linkedList.get(2));
		System.out.println(".element[3]: \t" + linkedList.get(3));
		System.out.println(".element[4]: \t" + linkedList.get(4));
		System.out.println(".element[5]: \t" + linkedList.get(5));

		System.out.println("The last element in list is: \t" + linkedList.searchLastIndex().getmValue());
		linkedList.addLastIndex();
		System.out.println(".element[6]: \t" + linkedList.get(6));
	}

}
