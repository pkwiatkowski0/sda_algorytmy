import java.util.Scanner;

public class Zadanie1_2_a {

	public static void main(String[] args) {
		System.out.println("Give an integer number: ");
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();

		System.out.println(Math.abs(number));

		sc.close();
	}

}
