import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class Zadanie1_3 {

	private static int result;

	public static void main(String[] args) {

		System.out.println("Give a char [s] -> days, hours, minutes, seconds to seconds: ");
		System.out.println("Give a char [d] -> seconds to number of: days, minutes, seconds: ");

		Scanner sc = new Scanner(System.in);
		String sign = sc.nextLine();
		switch (sign) {
		case "s": {
			int days = giveNumberOfDays(sc);
			int hours = giveNumberOfHours(sc);
			int minutes = giveNumberOfMinutes(sc);
			int secs = giveNumberOfSecs(sc);
			changeToSecs(days, hours, minutes, secs);
			System.out.println("Number of secs: " + result + " s");
		}
			break;
		case "d": {
			int hours;
			int minutes;

			System.out.println("give a number of secs: ");
			BigDecimal secs = sc.nextBigDecimal();

			// double days;

			// System.out.println( days - Math.floor( days ));

			// double d;
			BigDecimal days;
			// = new BigDecimal((d - Math.floor(d)) * 100);
			days = secs.divide(secs, 24);
			days = days.setScale(4, RoundingMode.HALF_DOWN);
			System.out.println(days.intValue());
			// days *= 86400;
			// hours *= 3600;
			// minutes *= 60;
			// result = days + hours + minutes + secs;
			// System.out.println(result);
		}
		default: {
			System.out.println("You gave a wrong character");
		}
		}
		sc.close();
	}

	private static int giveNumberOfSecs(Scanner sc) {
		System.out.println("give number of secs: ");
		int secs = sc.nextInt();
		return secs;
	}

	private static int giveNumberOfMinutes(Scanner sc) {
		System.out.println("give number of minutes: ");
		int minutes = sc.nextInt();
		return minutes;
	}

	private static int giveNumberOfHours(Scanner sc) {
		System.out.println("give number of hours: ");
		int hours = sc.nextInt();
		return hours;
	}

	private static int giveNumberOfDays(Scanner sc) {
		System.out.println("give number of days: ");
		int days = sc.nextInt();
		return days;
	}

	private static void changeToSecs(int days, int hours, int minutes, int secs) {
		days *= 86400;
		hours *= 3600;
		minutes *= 60;
		result = days + hours + minutes + secs;
	}
}
