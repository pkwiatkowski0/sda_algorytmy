
public class Lista {

	private Node mHead;
	private int mListSize;

	public void linkedList() {

		mHead = new Node(null);
		incrementListSize();

	}

	public void add(Object data) {
		if (mHead == null) {
			mHead = new Node(data);
		}

		Node temp = new Node(data);
		Node current = mHead;

		while (current.getmNext() != null) {
			current = current.getmNext();
		}
		// set the new node's next-node reference to this node's next-node
		// reference
		temp.setmNext(current.getmNext());
		// now set this node's next-node reference to the new node
		current.setmNext(temp);
		// increment the number of elements variable
		incrementListSize();
	}

	public void addLastIndex() {
			Node temp = new Node(mHead);
			Node current = mHead;
	 
			// Let's check for NPE before iterate over crunchifyCurrent
			if (current != null) {
				// crawl to the requested index or the last element in the list, whichever comes first
				for (int i = 0; i <  mListSize; i++) {
					current = current.getmNext();
				}
			}
	 
			// set the new node's next-node reference to this node's next-node reference
			temp.setmNext(current.getmNext());
	 
			// now set this node's next-node reference to the new node
			current.setmNext(temp);
	 
			// increment the number of elements variable
			incrementListSize();
		
	}

	public int getmListSize() {
		return mListSize;
	}

	public void incrementListSize() {
		this.mListSize++; // = ListSize;
		// mListSize++;
	}

	public void decrementListSize() {
		this.mListSize--; // = ListSize;
		// mListSize--;
	}

	public Node searchLastIndex() {
		Node temp = mHead;
		while (temp.getmNext() != null) {
			temp = temp.getmNext();

		}
		return temp;
	}

	public Object get(int index)
	// returns the element at the specified position in this list.
	{
		// index must be 1 or higher
		if (index < 0) {
			return null;
		}
		Node linkedList = null;
		if (mHead != null) {
			linkedList = mHead.getmNext();
			for (int i = 0; i < index; i++) {
				if (linkedList.getmNext() == null) {
					return null;
				}
				linkedList = linkedList.getmNext();
			}
			return linkedList.getmValue();
		}
		return linkedList;
	}

	public boolean remove(int index) {

		// if the index is out of range, exit
		if (index < 1 || index > mListSize) {
			return false;
		}

		Node current = mHead;
		if (mHead != null) {
			for (int i = 0; i < index; i++) {
				if (current.getmNext() == null) {
					return false;
				}

				current = current.getmNext();
			}
			current.setmNext(current.getmNext().getmNext()); // wskazujemy na
																// kolejny
																// element
																// przeskakując
																// (omijając)
																// dany element

			// decrement the number of elements variable
			decrementListSize();
			return true;

		}
		return false;
	}
}